package com.example.mediconatural;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

//clase para crear una cuenta en la aplicacion
public class activity_create_acount extends AppCompatActivity {
    private EditText textemail;
    private EditText textpassword;
    private EditText textpassword2;
    private Button crearIngresar;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_acount);
        firebaseAuth = FirebaseAuth.getInstance();
        crearIngresar = (Button)findViewById(R.id.btnCrearIngresar);
        textemail = (EditText)findViewById(R.id.textEmail);
        textpassword = (EditText)findViewById(R.id.textPassword);
        textpassword2 = (EditText)findViewById(R.id.textPassword2);
        progressDialog = new ProgressDialog(this);

        crearIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarIngresar();
            }
        });
    }

    private void registrarIngresar(){
        //leemos datos de las vistas
        final String email = textemail.getText().toString().trim();
        String password = textpassword.getText().toString();
        String password2 = textpassword2.getText().toString();
        //comprobamos los campos
        if(TextUtils.isEmpty(email)&&TextUtils.isEmpty(password)&&TextUtils.isEmpty(password2)){
            Toast.makeText(this,"LLene los campos vacios",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"se debe ingresar un email",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Falta ingresar una contraseña", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!TextUtils.equals(password, password2)){
            Toast.makeText(this,"Las contraseñas no coinsiden",Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Creando su cuenta, espere un momento...");
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(activity_create_acount.this, "Se ha registrado con exito", Toast.LENGTH_SHORT).show();
                        }else {
                            if(task.getException() instanceof FirebaseAuthUserCollisionException){
                                Toast.makeText(activity_create_acount.this, "El correo ingresado ya existe", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(activity_create_acount.this, "No se pudo registrar al usuario", Toast.LENGTH_SHORT).show();
                            }

                        }
                        progressDialog.dismiss();
                    }
                });
        loguearUsuario();
    }
    private  void loguearUsuario() {
        final String email = textemail.getText().toString().trim();
        String password = textpassword.getText().toString();

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        int pos = email.indexOf("@");
                        String user = email.substring(0,pos);
                        Toast.makeText(activity_create_acount.this, "Bienvenido: "+textemail.getText(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent (getApplication(), IntroActivity.class);
                        intent.putExtra(IntroActivity.user,user);
                        startActivity(intent);


                        progressDialog.dismiss();
                    }
                });
    }
}
