package com.example.mediconatural;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

//clase para loguearse
public class LoginActivity extends AppCompatActivity {
    private EditText textmail;
    private EditText textpassword;
    private TextView textirbiblioteca;
    private Button btnregistrar, btnIngresa;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();

        textmail = (EditText)findViewById(R.id.txtMail);
        textpassword = (EditText)findViewById(R.id.txtPassword);
        btnregistrar = (Button) findViewById(R.id.btnRegistrar);
        progressDialog = new ProgressDialog(this);
        btnIngresa = (Button) findViewById(R.id.btnIngresar);
        textirbiblioteca = (TextView) findViewById(R.id.guestMode);
        textirbiblioteca.setPaintFlags(textirbiblioteca.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        //elegimos la vista a la que iremos
        btnIngresa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loguearUsuario();
            }
        });
        btnregistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrarUsuario();
            }
        });
        textirbiblioteca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBiblioteca();
            }
        });

    }

    private void goBiblioteca(){
        Intent intent = new Intent (getApplication(), IntroActivity.class);
        startActivity(intent);
    }
    private void registrarUsuario(){
        Intent intent = new Intent (getApplication(), activity_create_acount.class);
        startActivity(intent);
    }
    private  void loguearUsuario() {

        final String email = textmail.getText().toString().trim();
        String password = textpassword.getText().toString();

        if(TextUtils.isEmpty(email)&&TextUtils.isEmpty(password)){
            Toast.makeText(this,"LLene los campos vacios",Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "se debe ingresar un email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Falta ingresar una contraseña", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Validando informacion, espere un momento...");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            int pos = email.indexOf("@");
                            String user = email.substring(0,pos);
                            Toast.makeText(LoginActivity.this, "Bienvenido: "+textmail.getText(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent (getApplication(), IntroActivity.class);
                            intent.putExtra(IntroActivity.user,user);
                            startActivity(intent);
                            //startActivityForResult(intent, 0);
                        } else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                Toast.makeText(LoginActivity.this, "El correo ingresado ya existe", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(LoginActivity.this, "Usuario no encontrado", Toast.LENGTH_SHORT).show();
                            }

                        }
                        progressDialog.dismiss();
                    }
                });
    }
}