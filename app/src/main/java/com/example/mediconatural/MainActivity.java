package com.example.mediconatural;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.example.mediconatural.Fragments.FragmentBiblioteca;
import com.example.mediconatural.Fragments.MainFragment;
import com.example.mediconatural.Fragments.sugerenciasFragment;
import com.google.android.material.navigation.NavigationView;

//esta es la clase principal en la que almacenamos nuestro toolbar,navigationbar,
// nuestro drawer layout y nuestros fragments de biblioteca y mainfragment
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private WebView webView;
    public static final String user="names";

    TextView txtUser;

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    Toolbar toolbar;
    NavigationView navigationView;
//variables para cargar el fragment principal
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer);
        navigationView = findViewById(R.id.navigationView);

        //establecer evento onclick al navigationview
        navigationView.setNavigationItemSelectedListener(this);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout,
                toolbar,
                R.string.open,
                R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        actionBarDrawerToggle.syncState();

        //cargar fragment principal xd
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container,new MainFragment());
        fragmentTransaction.commit();
        /**
        webView = (WebView) findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient());

        webView.loadUrl("https://web-chat.global.assistant.watson.cloud.ibm.com/preview.html?region=us-south&integrationID=7e1af6e5-425b-43de-9b59-309c4f372bd1&serviceInstanceID=8f2d74fd-a77c-429b-8e30-62325cd1c870");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        txtUser = (TextView)findViewById(R.id.txtuser);
        String user = getIntent().getStringExtra("names");
        txtUser.setText("Bienvenido: "+user); **/
    }

    //mensaje de alerta al presionar el boton back de android
    @Override
    public void onBackPressed() {
        AlertDialog.Builder myBuild = new AlertDialog.Builder(this);
        myBuild.setMessage("¿Está seguro que desea salir de la sesión?");
        myBuild.setTitle("¡Alerta!");
        myBuild.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        myBuild.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();            }
        });
        AlertDialog dialog = myBuild.create();
        dialog.show();
    }


    @Override
    //items de nuestro navigationbar
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        drawerLayout.closeDrawer(GravityCompat.START);
        if(menuItem.getItemId()==R.id.greenybotpage){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container,new MainFragment());
            fragmentTransaction.commit();
        }
        if(menuItem.getItemId()==R.id.bibliotecapage){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container,new FragmentBiblioteca());
            fragmentTransaction.commit();
        }
        if(menuItem.getItemId()==R.id.sugerenciaserrores){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container,new sugerenciasFragment());
            fragmentTransaction.commit();
        }
        if(menuItem.getItemId()==R.id.salir){
            onBackPressed();
        }
        return false;
    }

}
