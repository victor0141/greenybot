package com.example.mediconatural.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mediconatural.MyAdapter;
import com.example.mediconatural.R;

public class FragmentBiblioteca extends Fragment implements SearchView.OnQueryTextListener{
    SearchView svSearch;
    RecyclerView recyclerView;
    MyAdapter myAdapter;
    String s1[], s2[];
    int images[] = {R.drawable.jugoverde, R.drawable.jugoverde,R.drawable.jugoverde,
            R.drawable.jugoverde,R.drawable.jugoverde, R.drawable.jugoverde,
            R.drawable.jugoverde, R.drawable.jugoverde,
            R.drawable.jugoverde, R.drawable.jugoverde};
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        
        View view = inflater.inflate(R.layout.biblioteca_fragment,container,false);
        recyclerView = view.findViewById(R.id.reciclerView);

        s1 = getResources().getStringArray(R.array.biblioteca_medicinanatural);
        s2 = getResources().getStringArray(R.array.description);

        myAdapter = new MyAdapter(this.getContext(), s1, s2, images);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        return view;


    }
    public void initSearch(){
        svSearch=svSearch.findViewById(R.id.svSearch);
    }
    public void initListener() {
        svSearch.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        return false;
    }
}
