package com.example.mediconatural.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.mediconatural.MyWebViewClientGreeny;
import com.example.mediconatural.R;

public class MainFragment extends Fragment {

    private WebView webView;
    TextView txtUser;
    
    @SuppressLint("SetJavaScriptEnabled")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment,container,false);

        webView = (WebView) view.findViewById(R.id.webview);
        webView.setWebViewClient(new MyWebViewClientGreeny());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("https://web-chat.global.assistant.watson.cloud.ibm.com/preview.html?region=us-south&integrationID=7e1af6e5-425b-43de-9b59-309c4f372bd1&serviceInstanceID=8f2d74fd-a77c-429b-8e30-62325cd1c870");

     
        return view;
    }

}
