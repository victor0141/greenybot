package com.example.mediconatural;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

//esta clase hereda todas las propiedades de webClient y la usamos en el viewView (MainFragment)
public class MyWebViewClientGreeny extends WebViewClient {
    public boolean shuldOverrideKeyEvent (WebView view, KeyEvent event) {
        // Do something with the event here
        return true;
    }

    @Override
    //sobreescribimos este metodo para bloquear la pagina ibm
    public boolean shouldOverrideUrlLoading (WebView view, String url) {
        return url.equals("https://www.ibm.com/cloud/watson-assistant");
    }
}
