package com.example.mediconatural;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
//IntroActivity carga el viewPager id=viewpager de nuestra layout activity_intro
public class IntroActivity extends AppCompatActivity {
    ViewPager viewPager;
    public static final String user="names";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        viewPager = findViewById(R.id.viewpager);
        IntroAdapter adapter = new IntroAdapter(getSupportFragmentManager());
        //viewpager será nuestro adaptador de los fragmentos que pongamos
        viewPager.setAdapter(adapter);


    }

}