package com.example.mediconatural;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

//fragmento de nuestro tercer slider
public class ThirdFragment extends Fragment {
    TextView openActivity;
    TextView back;
    ViewPager viewPager;
    public ThirdFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_third, container, false);

        //inicia viewpager de introactivity
        viewPager = getActivity().findViewById(R.id.viewpager);

        openActivity = view.findViewById(R.id.slideOneNext);
        openActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                getActivity().startActivity(intent);
                //destruye la actividad en la que el fragment fue creado
                getActivity().finish();
            }
        });
        back = view.findViewById(R.id.slideOneBack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(1);
            }
        });

        return  view;

    }


}