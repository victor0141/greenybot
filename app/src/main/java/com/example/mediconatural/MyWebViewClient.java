package com.example.mediconatural;
import android.net.Uri;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
//esta clase hereda todas las propiedades de webClient y la usamos en el formulario de sugerencias
public class MyWebViewClient extends WebViewClient {
    public boolean shuldOverrideKeyEvent (WebView view, KeyEvent event) {
        // Do something with the event here
        return true;
    }

    @Override
    public boolean shouldOverrideUrlLoading (WebView view, String url) {
        return url.equals("https://formspark.io/");
    }
}
